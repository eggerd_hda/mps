# Mikroprozessorsysteme
*2\. Semester, 2014*  
*Prof. Dr. Klaus Frank*

Im Rahmen dieses Praktikum haben wird u. A. den Umgang mit Peripherie und Softwareinterrupts gelernt und das Gelernte anschließend dazu verwendet um die Steuerung einer Ausschankstation, bestehend aus einer Saitenresonanzwaage und einer Pumpe, zu programmieren.


## Behandelte Themen
- Parallele & serielle Schnittstellen
- Programmierung 
- Softwareinterrupts & Interrupt Handling
- C-Programmierung für eingebettete Systeme
- Umgang mit Peripherie in eingebetteten Systemen
- Realisierung von Zeitmessungen mit Hilfe von Zählern im Capture-Mode
- Bedeutung und Anwendung von Supervisor- und User-Mode
- Timer (WAVE & Capture Mode)


## Anforderungen
- Nach dem Starten des Programms soll eine Begrüßung und Informationen über die serielle Schnittstelle ausgegeben werden
- Nach Aufstellen eines Gefäßes und Drücken der Taste `SW1` soll die Waage kalibriert und das Gefäßgewicht ausgegeben werden
- Nach drücken der Taste `SW2` soll der Pumpvorgang gestartet werden und eine vorprogrammierte Menge abgefüllt werden
- Der Pumpvorgang soll automatisch abgebrochen werden, sollte das Gefäß frühzeitig von der Waage genommen werden
- Da die Waage sehr empfindlich ist, muss mit leichten Gewichtsschwankungen gerechnet werden
- Nachdem der Pumpvorgang abgeschlossen wurde, soll die abgefüllte Menge und das neue Gewicht des Gefäßes ausgegeben werden
- Mehrere Pumpvorgänge mit unterschiedlichen Gefäßen sollen ohne Neustart des Programms möglich sein
- Es soll eine Dokumentation des Programms angefertigt werden
    - Ausführliche Funktionsbeschreibungen
    - Installationsanleitung
    - Benutzerhandbuch


## Endergebnis
[![Demo GIF](https://gitlab.com/eggerd_hda/mps/-/raw/assets/demo.gif)](https://gitlab.com/eggerd_hda/mps/-/blob/assets/demo.mp4)
- Das Programm erlaubt eine Abweichung von bis zu 3g zwischen einzelnen Gewichtsmessungen, um Gewichtsschwankungen der Wage abzufangen
    - Eine undokumentierte Anforderung an das Programm war es, dass der Pumpvorgang nicht unterbrochen wird, sollte das Gefäß mit einer Papierkugel abgeworfen werden oder jemand leicht gegen den Tisch der Waage stoßen
    - Diese Toleranz wird zudem dazu verwendet um festzustellen ob das Gefäß von der Waage genommen wurde
- Nach Abschluss eines Pumpvorgangs kann dieser jederzeit neu gestartet werden, indem zuvor eine erneute Kalibrierung durch Drücken der `SW1` Taste ausgeführt wird
- Siehe [Dokumentation](https://gitlab.com/eggerd_hda/mps/-/blob/master/Dokumentation.pdf)


## Beteiligte Personen
- David Steurer
- [Dennis Müßig](https://gitlab.com/demues)
- [Dustin Eckhardt](https://gitlab.com/eggerd)
- Jeffrey Papke


## Verwendete Hardware
- Saitenresonanzwaage
- [AT91 ARM Thumb Microcontrollers](http://ww1.microchip.com/downloads/en/devicedoc/doc1354.pdf)
- WaSim (Simulator für Saitenresonanzwage und Pumpensignal, zum Testen an den Arbeitsplätzen)