// Praktikum durchgeführt von:
// Dustin Eckhardt & Dennis Müßig - in Kooperation mit Jeffrey Papke & David S.

//Initialisierung
#include "../h/pio.h"
#include "../h/tc.h"
#include "../h/pmc.h"
#include "../h/aic.h"

#define TC3_INIT  TC_ACPC_CLEAR_OUTPUT | TC_ACPA_SET_OUTPUT | TC_WAVE | TC_CPCTRG | TC_CLKS_MCK8
#define TC4_INIT  TC_CLKS_MCK2 | TC_LDBSTOP | TC_CAPT | TC_LDRA_RISING_EDGE | TC_LDRB_RISING_EDGE
#define TC5_INIT  TC_CLKS_MCK2 | TC_LDBSTOP | TC_CAPT | TC_LDRA_RISING_EDGE | TC_LDRB_RISING_EDGE

//Variablen
const volatile int C1 = 2000;
const volatile int C2 = 0;
volatile int action = 1;			// gibt an welche Aktion vom Programm gerade durchgeführt werden soll

// Prototypen
void intToString(int);										// gibt eine Integer-Zahl auf der Konsole aus
void Timer_Init(void);										// initialisiert den Timer für die Konsolenausgabe
int MessungDerMasse(void);									// misst die Masse, die zurzeit auf der Waage liegt
void Timer3_Init(void);										// initialisiert den Timer zum pumpen
void taste_irq_handler(void)__attribute__((interrupt));		// Interrupt-Routine zum Bediehnen der Tasten (SW1 & SW2)



int main(void)
{
	int cup = 0, masse = 0, MaxPumpwert = 50, pumpCounter = 0, loading = 0, letzteMasse, error = 0;
	
	StructPMC* pmcbase = PMC_BASE;
	StructPIO* piobaseB = PIOB_BASE;
	StructPIO* piobaseA = PIOA_BASE;
	StructAIC* aicbase = AIC_BASE;
	StructTC* timerbase3 = TCB3_BASE;
	pmcbase->PMC_PCER = 0x06C00;
	// Anschalten fuer: Piobase A/B - Timer 3/4/5
	
	piobaseB->PIO_IER = KEY1 | KEY2;
	piobaseB->PIO_ODR = KEY1 | KEY2;

	aicbase->AIC_IDCR = 1<<PIOB_ID;
	aicbase->AIC_SVR[PIOB_ID] = (unsigned int) taste_irq_handler;
	aicbase->AIC_SMR[PIOB_ID] = 0x7;
	aicbase->AIC_IECR = 1<<PIOB_ID;
	piobaseB->PIO_IER = KEY1 | KEY2;

	inits();
	Timer_Init();
	Timer3_Init();
	pmcbase->PMC_PCDR = 0x200;

	while(1) // Dauerschleife für das Menü
	{
		switch(action)
		{
			case 1:	// Startzustand	des Programms
				// Willkommensnachricht und Informationen ausgeben
				puts ("\n\r-= Herzlich Willkommen: =- \n \r");
				puts ("[1.] Druecken Sie die Taste SW1 um die Waage zu kalibrieren\n \r");
				puts ("[2.] Druecken Sie die Taste SW2 um das Abfuellen zu starten \n \r");
				puts ("[2.1] Die Waage hat ein maximum von 1000kg welches nicht ueberschritten werden darf! \n \r");
				puts ("[3.] Die Pumpe hat eine Leistung von 50 Hz/sek. \n \n \r");
				
				action = 0; // Programm auf Leerlauf stellen
			break;
		
			case 2: // Kalibrierung des Programms
				cup = MessungDerMasse(); // aktuelle Masse auf der Wage berechnen und diese in "cup" speichern (wird später noch verwendet!)

				if(cup >= 999) // falls das maximale Gewicht erreicht wurde
				{
					puts ("Das Gewicht darf 1000 Gramm nicht ueberschreiten!\n\r");					
				}			
				else
				{
					// aktuelles Gewicht ausgeben
					puts ("Der Becher wiegt: ");
					intToString(cup);
					puts (" Gramm\n\r");
					
					if(cup <= 1) // falls ohne Gewicht kalibriert wird (Tolleranz +1g), den "pumpCounter" resetten
					{
						pumpCounter = 0;
					}
				}
									
				action = 0; // in Leerlauf schalten					
			break;
			
			case 3:
				if(cup > 1) // nur wenn kalibriert wurde und (vermutlich) ein Becher auf der Waage steht
				{
					masse = MessungDerMasse(); // aktuelle Masse berechnen

					if(masse <= 999) // solange die Masse das Maximum nicht erreicht hat
					{
						if((masse - cup) < MaxPumpwert) // solange die zu pumpende Masse nicht erreicht wurde - sonst muss erst neu kalibriert werden
						{
							piobaseB->PIO_IDR = KEY1 | KEY2; // Taste 1 & 2 für die Dauer des Pumpvorgangs deaktivieren (werden in diesem Zeitraum nicht benötigt)
							
							while((masse - cup) < MaxPumpwert) // bis die zu pumpende Masse erreicht wurde
							{
								// Anzeige für den laufenden Pumpvorgang
								if(loading < 10)
								{
									puts("   pumpt.. /\r");
									loading++;
								}
								else if(loading < 20)
								{
									puts("   .pumpt. -\r");
									loading++;
								}
								else if(loading < 30)
								{
									puts("   ..pumpt \\\r");
									loading++;
								}
								else if(loading < 40)
								{
									puts("   t..pump |\r");
									loading++;
								}
								else if(loading < 50)
								{
									puts("   pt..pum /\r");
									loading++;
								}
								else if(loading < 60)
								{
									puts("   mpt..pu -\r");
									loading++;
								}
								else if(loading < 70)
								{
									puts("   umpt..p \\\r");
									loading++;
								}
								else
								{
									puts("   ####### |\r");
									loading = 0;
								}
								


								pmcbase->PMC_PCER = 0x200;
								piobaseA->PIO_PDR = ( 1 << PIOTIOA3);
								
								masse = MessungDerMasse(); // aktuelle Masse berechnen
								letzteMasse = masse; // Speichert die Masse des jeweils letzen Durchlaufs
																
								if(masse >= 999) // falls während des Pumpvorgangs das Maximum erreicht wird
								{
									puts("Die Waage hat das Maximalgewicht erreicht!\n\r"); // Fehlermeldung ausgeben
									error++; // Error-Counter erhöhen
									break; // Schleife beenden
								}
								else if((letzteMasse - (masse + 3)) > 0) // falls die Masse im Vergleich zum letzten Durchlauf weniger wurde (Tolleranz = -3g)
								{
									puts("Vorgang unterbrochen, da der Becher entfernt wurde!\n\r"); // Fehlermeldung ausgeben
									error++; // Error-Counter erhöhen
									break; // Schleife beenden
								}																									
							}
							
							pmcbase->PMC_PCDR = 0x200;
							piobaseA->PIO_PER = ( 1 << PIOTIOA3);
						
							// neues Gesamtgewicht (inkl. Becher) ausgeben
							puts("Gesamtgewicht nach dem Pumpen: ");
							intToString(masse);
							puts(" Gramm.\n\r");
						
							// insgesamt gepumpte Menge ans Wasser ausgeben
							puts("Insgesamt gepumpte Menge: ");						
							pumpCounter += (letzteMasse - cup); // pumpCounter um zuletzte gepumpte Masse erhöhen (jeweils ohne Gewicht des Bechers)
							intToString(pumpCounter);
							puts(" Gramm.\n\r");
							
							letzteMasse = 0; // letzte Masse leeren
							
							if(error > 0) // falls während des pumpens Fehler aufgetreten sind
							{
								action = 1; // Programm in Startzustand versetzen (Willkommensnachricht)
								error = 0; // Error-Counter resetten
							}
							else
							{
								action = 0; // falls keine Fehler aufgerteten sind wechselt das Programm in den Leerlauf und wartet auf die Eingabe eines Benutzers
							}
							
							piobaseB->PIO_IER = KEY1 | KEY2; // Taste 1 & 2 wieder aktivieren
						}
						else // offenbar wurde bereits einmal gepumpt - das Programm muss zunächst neu kalibriert werden um das aktuelle Bechergewicht zu erhalten
						{							
							puts("Die Waage muss erst neu kalibriert werden!\n\r");
							action = 1; // Programm in Startzustand versetzen
						}
					}
					else // das Maximum wurde erreicht
					{
						action = 1; // Programm in Startzustand versetzen
						puts("Die Waage hat das Maximalgewicht erreicht!\n\r");
					}
				}
				else // auf der Waage befindet sich kein Becher
				{
					puts("Stellen Sie zunaechst einen Becher auf die Waage\n\r");					
					action = 0; // Programm in den Leerlauf schalten - wartet auf Eingabe durch Benutzer
				}									
			break;
			
			default: break; // Leerlauf - Programm wartet auf Tastendruck durch Benutzer
		}	
	}
}



void taste_irq_handler(void)
{
	StructPIO* piobaseB = PIOB_BASE;
	StructAIC* aicbase = AIC_BASE;
	StructPMC* pmcbase = PMC_BASE;		
	
	if((piobaseB->PIO_PDSR & KEY1) == 0) // wenn Taste 1 (SW1) gedrückt wird
	{
		action = 2; // Start der Kalibrierung
	}
	else if ((piobaseB->PIO_PDSR & KEY2) == 0) // wenn Taste 2 (SW2) gedrückt wird
	{
		action = 3;	// Start des Pumpvorgangs
	}
	
	volatile int i;
	for(i = 0; i < 100000; i++){} // Time-waster um das mehrfache reagieren auf einen Tastendruck zu verhindern
	
	aicbase->AIC_EOICR = piobaseB->PIO_ISR;	// Interrupt beenden
}	



void Timer3_Init(void)
{
	StructTC* timerbase3 	= 	TCB3_BASE;
	StructPIO* piobaseA 	= 	PIOA_BASE;
	StructPMC* pmcbase	= 	PMC_BASE;
	
	pmcbase->PMC_PCER 	= 	0x200;
	timerbase3->TC_CCR 	= 	TC_CLKDIS;			// Clock disable
	piobaseA->PIO_PDR 	=	(1 << PIOTIOA3);
	timerbase3->TC_CMR 	=	TC3_INIT;
	
	timerbase3->TC_RA	=	31250;
	timerbase3->TC_RC	=	62500;
	
	timerbase3->TC_CCR	=	TC_CLKEN;			// Clock enable
	timerbase3->TC_CCR	=	TC_SWTRG;
	piobaseA->PIO_PER	=	(1<<PIOTIOA3);
	piobaseA->PIO_OER	=	(1<<PIOTIOA3);
	piobaseA->PIO_CODR	=	(1<<PIOTIOA3);
	pmcbase->PMC_PCDR	=	0x200;
}



void Timer_Init(void)
{
	StructPIO* piobaseA = PIOA_BASE;
	StructTC* tcbase4 = TCB4_BASE;
	StructTC* tcbase5 = TCB5_BASE;
	
	piobaseA->PIO_PDR	=	0x090;		// ausschalten der PIOS (0x090 = 10010000 B)
	tcbase4->TC_CCR		=	TC_CLKDIS;	// Channel Controll Register = Clock Disable 
	tcbase4->TC_CMR		=	TC4_INIT;	// Clock Channel Mode = TC4_Init (0x1) [ So wie das TMC ]
	tcbase4->TC_CCR		=	TC_CLKEN;	// Channel Controll Register = Clock Enablen
	tcbase4->TC_CCR		=	TC_SWTRG;	// Channel Controll Register = Swagtrigger
	
	tcbase5->TC_CCR		=	TC_CLKDIS;	// ^ siehe obere Beschreibung
	tcbase5->TC_CMR		=	TC5_INIT;	// ^
	tcbase5->TC_CCR		=	TC_CLKEN;	// ^
	tcbase5->TC_CCR		=	TC_SWTRG;	// ^
}



int MessungDerMasse(void)
{
	// deklaration der benötigten Variablen
	volatile int captureRA1;
	volatile int captureRB1;
	volatile int capturediff1;
	volatile float Periodendauer1;
	volatile float Periodendauer2;
	volatile float Masse;

	// Timerbase deklarieren
	StructTC* tcbase4 = TCB4_BASE;
	StructTC* tcbase5 = TCB5_BASE;
	
	volatile	int i;
	for(i = 0; i < 3; i++) // Berechnung der Masse drei mal durchführen um genaueren Wert zu erhalten
	{	
		// Capture Register lesen um Reset zu erhalten
		tcbase4->TC_SR;
		tcbase5->TC_SR; 
		
		// Timer starten
		tcbase4->TC_CCR	= TC_SWTRG;
		tcbase5->TC_CCR	= TC_SWTRG;
			
		while (!(tcbase4->TC_SR & 0x40));							// Capture Register B wurde geladen Messung abgeschlossen
			captureRA1	= tcbase4->TC_RA;							// Messen von RA1 = Wert von RA aus Timerbase 4  	
			captureRB1	= tcbase4->TC_RB;							// Messen von RB1 = Wert von RB aus Timerbase 4
			capturediff1	= abs(captureRB1) - abs(captureRA1);	// Differenz berechnen 
			Periodendauer1 = abs(capturediff1);						// Zeit in us
			
		while (!(tcbase5->TC_SR & 0x40));							// ^ siehe obere Beschreibung
			captureRA1	= tcbase5->TC_RA;							// ^
			captureRB1	= tcbase5->TC_RB;							// ^
			capturediff1	= abs(captureRB1) - abs(captureRA1);	// ^
			Periodendauer2 = abs(capturediff1);						// ^
	}
					
	Masse = C1 * ((Periodendauer1 / Periodendauer2) - 1 ) - C2; // Masse berechnen
	Masse += 0.5; // Masse um 0,5 erhöhen um Rundungsfehler zu verhindern

	return Masse;
}



void intToString(int input)
{
	int arySize = 9;		// Array Größe definieren
	char string[arySize];	// Array initialisieren
	int aryPos = 0;			// Array Position, an die geschrieben wird, initialisieren
	
	char temp[arySize];		// Array zur zwischenablage initialisieren	
	int aryTempPos = 0;		// Array Position, an die geschrieben wird, initialisieren
	temp[0] = '0';			// erste Stelle mit Null initialisieren, falls später ein Fehler auftritt
	

	if(input < 0)			// falls Input kleiner als 0
	{
		string[aryPos++] = '-';		// Position 0 erhält das "-" Zeichen
		input *= -1;				// Zahl invertieren
	}

	
	while(input > 0 && aryTempPos < (arySize - 1)) // solange die Zahl größer Null ist und die maximale Arraylänge nicht erreicht wurde
	{	
		int mod = (input % 10);			// Einerstelle der Zahl errechnen
		char var = (char)(mod + 48);	// ASCII 48-57 sind Zahlen - daher 48 + die errechnete Zahl = ASCII Code für die errechnete Zahl
		temp[aryTempPos++] = var;		// Char in der Zwischenablage speichern und anschließend den Posistions-Counter erhöhen
		input /= 10;					// durch Division die eben verwendete Einerstelle wegfallen lassen
	}
	
	if(aryTempPos - 1 >= 0){aryTempPos--;} // falls die Zwischenablage Zeichen enthält, Positions-Counter um eins verringern (um an Position des letzten Zeichens zu gelangen)

	while(aryTempPos >= 0 && aryPos < (arySize - 1)) // solange nicht alle Positionen der Zwischenablage abgearbeitet wurden und die maximale Arraylänge nicht erreicht wurde
	{
		char var = temp[aryTempPos--];	// letztes Zeichen aus der Zwischenablage holen und Positions-Counter verringern
		string[aryPos++] = var;			// Zeichen an aktueller Position speichern und Positions-Counter erhöhen
	}
	
	while(aryPos < arySize)	// falls noch Positionen im Array frei sind
	{
		string[aryPos++] = (char)0;	// mit "\0" auffüllen um Fehler zu vermeiden
	}		
	
	puts(string); // umgewandelte Zahl an "puts"-Funktion übergeben
}

